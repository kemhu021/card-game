// EE285 - Project 1 - By: Kassem Qamhieh & Emma Heightoff
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

int main(void) {

srand(time(0)); // Generates a random seed each run

int Card1 = (rand() % (13 - 1 + 1)) + 1; // Random generation of card 1
int Card2 = (rand() % (13 - 1 + 1)) + 1; // Random generation of card 2
int Suite1 = (rand() % (4 - 1 + 1)) + 1; // Random generation of suite of card 1
int Suite2 = (rand() % (4 - 1 + 1)) + 1;  // Random generation of suite of card 2
int AceDouble = 11; // Handles the double value of the ace card
int UserGuess; // User's guess variable
int ValC1; // For storing point value of card 1
int ValC2; // For storing point value of card 2
int Calc1; // Stores calculation one for comparison 
int Calc2; // Stores calculation two for comparison

printf("\n\nWelcome to group 40's card game!\n\nDrawing first card...\n\n");
sleep(3); // pause for ease of reading

// Beginning of first card selection

if (Card1 == 1) { // This if statement prints the value of the second card
    printf("Your first card is the Ace of ");
    ValC1 = 1;
}
if (Card1 == 2) {
    printf("Your first card is the Two of ");
    ValC1 = 2;
}
if (Card1 == 3) {
    printf("Your first card is the Three of ");
    ValC1 = 3;
}
if (Card1 == 4) {
    printf("Your first card is the Four of ");
    ValC1 = 4;
}
if (Card1 == 5) {
    printf("Your first card is the Five of ");
    ValC1 = 5;
}
if (Card1 == 6) {
    printf("Your first card is the Six of ");
    ValC1 = 6;
}
if (Card1 == 7) {
    printf("Your first card is the Seven of ");
    ValC1 = 7;
}
if (Card1 == 8) {
    printf("Your first card is the Eight of ");
    ValC1 = 8;
}
if (Card1 == 9) {
    printf("Your first card is the Nine of ");
    ValC1 = 9;
}
if (Card1 == 10) {
    printf("Your first card is the Ten of ");
    ValC1 = 10;
}
if (Card1 == 11) {
    printf("Your first card is the Jack of ");
    ValC1 = 10;
}
if (Card1 == 12) {
    printf("Your first card is the Queen of ");
    ValC1 = 10;
}
if (Card1 == 13) {
    printf("Your first card is the King of ");
    ValC1 = 10;
}

if (Suite1 == 1) { // This if statement prints the suite of the first card
    printf("Spades.\n\n");
}
if (Suite1 == 2) {
    printf("Hearts.\n\n");
}
if (Suite1 == 3) {
    printf("Diamonds.\n\n");
}
if (Suite1 == 4) {
    printf("Clubs.\n\n");
}

// End of first card selection

sleep(3);
printf("Please enter a number you believe may be the sum of your two cards,\nor the absolute value of the difference of your two cards.\n\n");
scanf("%d", &UserGuess); // grabs user's guess
printf("\nDrawing second card...\n\n");
sleep(3);

// Beginning of second card selection

if (Card2 == 1) { // This if statement prints the value of the second card
    printf("Your second card is the Ace of ");
    ValC2 = 1;
}
if (Card2 == 2) {
    printf("Your second card is the Two of ");
    ValC2 = 2;
}
if (Card2 == 3) {
    printf("Your second card is the Three of ");
    ValC2 = 3;
}
if (Card2 == 4) {
    printf("Your second card is the Four of ");
    ValC2 = 4;
}
if (Card2 == 5) {
    printf("Your second card is the Five of ");
    ValC2 = 5;
}
if (Card2 == 6) {
    printf("Your second card is the Six of ");
    ValC2 = 6;
}
if (Card2 == 7) {
    printf("Your second card is the Seven of ");
    ValC2 = 7;
}
if (Card2 == 8) {
    printf("Your second card is the Eight of ");
    ValC2 = 8;
}
if (Card2 == 9) {
    printf("Your second card is the Nine of ");
    ValC2 = 9;
}
if (Card2 == 10) {
    printf("Your second card is the Ten of ");
    ValC2 = 10;
}
if (Card2 == 11) {
    printf("Your second card is the Jack of ");
    ValC2 = 10;
}
if (Card2 == 12) {
    printf("Your second card is the Queen of ");
    ValC2 = 10;
}
if (Card2 == 13) {
    printf("Your second card is the King of ");
    ValC2 = 10;
}

if (Suite2 == 1) { // This if statement prints the suite of the second card
    printf("Spades.\n\n");
}
if (Suite2 == 2) {
    printf("Hearts.\n\n");
}
if (Suite2 == 3) {
    printf("Diamonds.\n\n");
}
if (Suite2 == 4) {
    printf("Clubs.\n\n");
}

// End of second card selection

// Section for calculating results and declaring outputs

sleep(3); // This section checks to see if the user guess matched any possible summations 
Calc1 = ValC1 + ValC2;
if (Calc1 == UserGuess) { 
    printf("Your guess of %d, was correct for the summation of your two cards!\n\n", UserGuess);
}
if (Calc1 != UserGuess && Card1 == 1) {
    Calc1 = AceDouble + ValC2;
    if (Calc1 == UserGuess) {
        printf("Your guess of %d, was correct for the summation of your two cards!\n\n", UserGuess);
    }
}
if (Calc1 != UserGuess && Card2 == 1) { 
    Calc1 = ValC1 + AceDouble;
    if (Calc1 = UserGuess) {
        printf("Your guess of %d, was correct for the summation of your two cards!\n\n", UserGuess);
    }    
}
if (Calc1 != UserGuess && Card1 == 1 && Card2 == 1) {
    Calc1 = AceDouble + AceDouble;
    if (Calc1 == UserGuess) {
        printf("Your guess of %d, was correct for the summation of your two cards!\n\n", UserGuess);
    }    
} 
else {
    printf("Your guess of %d, was not correct for summation of your two cards.\n\n", UserGuess);
    sleep(3);
} // This section checks to see if the user guess matched any possible summations

if (Calc1 != UserGuess) { // This section checks to see if the user guess matched any possible absolute values of the difference of two cards
    Calc2 = abs(ValC1 - ValC2);
    if (Calc2 == UserGuess) {
        printf("Your guess of %d, was correct for absolute value of the difference of your two cards.\n\n", UserGuess);
    }
    else {
        Calc2 = abs(ValC2 - ValC1);
        if (Calc2 == UserGuess) {
            printf("Your guess of %d, was correct for absolute value of the difference of your two cards.\n\n", UserGuess);
    }
}
if (Calc2 != UserGuess && Card1 == 1) {
    Calc2 = abs(AceDouble - ValC2);
    if (Calc2 == UserGuess) {
        printf("Your guess of %d, was correct for absolute value of the difference of your two cards.\n\n", UserGuess); 
    }
    if (Calc2 != UserGuess) {
        Calc2 = abs(ValC2 - AceDouble);
        if (Calc2 == UserGuess) {
            printf("Your guess of %d, was correct for absolute value of the difference of your two cards.\n\n", UserGuess);
        }
    }
}
if (Calc2 != UserGuess && Card2 == 1) {
    Calc2 = abs(ValC1 - AceDouble);
    if (Calc2 == UserGuess) {
        printf("Your guess of %d, was correct for absolute value of the difference of your two cards.\n\n", UserGuess);
    }
    if (Calc2 != UserGuess) {
        Calc2 = abs(AceDouble - ValC1);
        if (Calc2 == UserGuess) {
            printf("Your guess of %d, was correct for absolute value of the difference of your two cards.\n\n", UserGuess);
        }
    }
}
if (Calc2 != UserGuess && Card1 == 1 && Card2 == 1) {
    Calc2 = abs(AceDouble - AceDouble);
    if (Calc2 == UserGuess) {
        printf("Your guess of %d, was correct for absolute value of the difference of your two cards.\n\n", UserGuess);
    }
}
else {
    printf("Your guess of %d, was not correct for the absolute value of the difference of your two cards.\n\n", UserGuess);
} // This section checks to see if the user guess matched any possible absolute values of the difference of two cards

return 0;

}
}